[![Netlify Status](https://api.netlify.com/api/v1/badges/95659ed5-f41a-4a90-af9c-32a8aa958084/deploy-status)](https://app.netlify.com/sites/deconst-next/deploys)

# Deconst documentation

Documentation for the *deconst* project: a continuous delivery pipeline
for heterogenous documentation. You can read the documentation at 
[deconst-next.org](https://deconst-next.org/).

## Building

To build this documentation on your local machine, use the [Deconst
integrated build](https://gitlab.com/deconst-next/integrated). Use the Sphinx preparer
and a clone of `https://gitlab.com/deconst-next/docs-control` as the control
repository.


# Deconst Dev Env in Kubernetes with Minikube

These instructions will prepare and submit the content and assets for this
deconst documentation in a dev env in Kubernetes with Minikube.

1. If necessary, deploy the [presenter service](https://gitlab.com/deconst-next/presenter#deconst-dev-env-in-kubernetes-with-minikube)

1. Open a new shell

1. Prepare the content

    ```bash
    wget https://gitlab.com/deconst-next/preparer-sphinx/raw/master/deconst-preparer-sphinx.sh
    chmod u+x deconst-preparer-sphinx.sh

    ./deconst-preparer-sphinx.sh

    ls _build/deconst-envelopes/
    ls _build/deconst-assets/
    ```

1. Submit the content

    The `CONTENT_SERVICE_APIKEY` must match the `ADMIN_APIKEY` set when
    deploying the [content
    service](https://gitlab.com/deconst-next/content-service#deconst-dev-env-in-kubernetes-with-minikube).

    ```bash
    export CONTENT_SERVICE_URL=$(minikube service --url --namespace deconst content)
    export CONTENT_SERVICE_APIKEY=$ADMIN_APIKEY

    wget https://gitlab.com/deconst-next/submitter/raw/master/deconst-submitter.sh
    chmod u+x deconst-submitter.sh

    ./deconst-submitter.sh
    ```

1. Prepare the staging content

    The [staging environment](https://deconst-next.org/developing/staging/) is a
    specially configured content service and presenter pair that allow users to
    preview content. Normally the Strider server will push preview content to
    the staging environment but this is how you would manually do it.

    ```bash
    export CONTENT_ID_BASE=https://gitlab.com/staging/deconst-next/deconst-docs/
    export ENVELOPE_DIR="_build/deconst-staging-envelopes"
    export ASSET_DIR="_build/deconst-staging-assets"

    ./deconst-preparer-sphinx.sh

    ls _build/deconst-staging-envelopes/
    ls _build/deconst-staging-assets/
    ```

1. Submit the staging content

    ```bash
    export CONTENT_SERVICE_URL=$(minikube service --url --namespace deconst staging-content)
    export ENVELOPE_DIR="$(pwd)/_build/deconst-staging-envelopes"
    export ASSET_DIR="$(pwd)/_build/deconst-staging-assets"

    ./deconst-submitter.sh
    ```

1. Confirm the content envelopes are in mongo

    ```bash
    kubectl run --namespace deconst --rm -it mongo-cli --image=mongo:2.6 --restart=Never -- mongo mongo.deconst.svc.cluster.local
    show dbs
    use content
    db.envelopes.count()
    db.staging_envelopes.count()
    ```

1. Deploy the [deconst control repo](https://gitlab.com/deconst-next/docs-control#deconst-dev-env-in-kubernetes-with-minikube)

1. Recreate the content DB

    The content is stored in the mongo DB files at `/data/deconst/mongo` in the
    minikube VM. Delete the DB files and the mongo pod. Kubernetes will
    automatically restart the mongo pod.

    ```bash
    minikube ssh
    sudo rm -rf /data/deconst/mongo/*
    exit
    MONGO_POD_NAME=$(kubectl get pods --selector name=mongo --output=jsonpath={.items..metadata.name} --namespace deconst)
    kubectl delete po/$MONGO_POD_NAME --namespace deconst
    ```

    Now you can run through the instructions above again to prepare and submit
    the content.
